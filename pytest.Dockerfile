FROM python:3.6-slim
COPY /app/api/tests /python-test
WORKDIR /python-test
RUN pip freeze > requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install pytest
CMD tail -f /dev/null
