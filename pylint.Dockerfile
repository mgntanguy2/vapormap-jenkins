FROM python:3.6-slim
COPY /app /python-test
WORKDIR /python-test
RUN pip freeze > requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install pylint
CMD tail -f /dev/null
